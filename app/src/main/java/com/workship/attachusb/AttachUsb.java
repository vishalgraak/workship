package com.workship.attachusb;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.workship.R;
import com.workship.Utils.Utils;
import com.workship.sensor.MyHandler;
import com.workship.sensor.SlideStartActivity;
import com.workship.sensor.UsbService;

import java.util.Set;

public class AttachUsb extends AppCompatActivity {
    public static UsbService usbService;
    private MyHandler mHandler;

    private Utils mUtils;
    public static String strAlcoValue;
    public static boolean flagAlValue;
    private TextView mMsgView;
    private ImageView attcHardImg;
    public static Bitmap mFinalBitmap;
    public final ServiceConnection usbConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {


            usbService = ((UsbService.UsbBinder) arg1).getService();
            mHandler = new MyHandler(AttachUsb.this);
            usbService.setHandler(mHandler);
            mUtils = new Utils(AttachUsb.this);

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            usbService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_attach_usb);
        mMsgView = (TextView) findViewById(R.id.attach_msg_txt);
        attcHardImg=(ImageView)findViewById(R.id.attach_hardware_img) ;
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Start listening notifications from UsbService
        startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it

    }

    @Override
    public void onResume() {
        super.onResume();
        setFilters();
    }

    @Override
    public void onPause() {
        super.onPause();
        //  unregisterReceiver(mUsbReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mUsbReceiver);
        unbindService(usbConnection);
    }

    public void setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }

    public void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
        if (!UsbService.SERVICE_CONNECTED) {
            Intent startService = new Intent(this, service);
            if (extras != null && !extras.isEmpty()) {
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    /*
    * Notifications from UsbService will be received here.
    */
    public final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                    Utils.openActivity(SlideStartActivity.class);
                    Toast.makeText(context, "USB Ready.", Toast.LENGTH_SHORT).show();
                    attcHardImg.setVisibility(View.GONE);
                    break;
                case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                    mMsgView.setText("USB Permission not granted!\n Please Give Permission to Record Value.");
                    Toast.makeText(context, "USB Permission not granted.", Toast.LENGTH_SHORT).show();
                    attcHardImg.setVisibility(View.VISIBLE);
                    break;
                case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    mMsgView.setText("Please Connect to Sensor!");
                    attcHardImg.setVisibility(View.VISIBLE);
                    break;
                case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                    mMsgView.setText("Please Connect to Sensor!");
                    attcHardImg.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "USB disconnected.", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                    mMsgView.setText("USB device not supported!");
                    attcHardImg.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "USB device not supported", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
