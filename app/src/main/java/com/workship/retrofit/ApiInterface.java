package com.workship.retrofit;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @POST("webservice.php")
    @FormUrlEncoded
    Call<LoginModel> getLoginDetails(@Field("login") String login,
                                     @Field("email") String email,
                                     @Field("password") String password);
    @Multipart
    @POST("/api/Accounts/editaccount")
    Call<LoginModel> editUser ( @Part("user_image") RequestBody file , @Part("alco_value") RequestBody fname);

    @GET("movie/{id}")
    Call<LoginModel> getSignUpData(@Path("id") int id, @Query("api_key") String apiKey);
}