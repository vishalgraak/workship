package com.workship.retrofit;

import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("status")
    public Integer mStatus;
    @SerializedName("message")
    public String mMessage;
}