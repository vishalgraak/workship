package com.workship.sensor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.romychab.slidetounlock.ISlideListener;
import com.github.romychab.slidetounlock.SlideLayout;
import com.github.romychab.slidetounlock.renderers.ScaleRenderer;
import com.github.romychab.slidetounlock.sliders.HorizontalSlider;
import com.workship.AlcoDetail.AlcoDetail;
import com.workship.R;
import com.workship.Utils.Utils;
import com.workship.attachusb.AttachUsb;
import com.workship.login.LoginActivity;
import com.workship.retrofit.ApiClient;
import com.workship.retrofit.ApiInterface;
import com.workship.retrofit.LoginModel;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.workship.attachusb.AttachUsb.flagAlValue;
import static com.workship.attachusb.AttachUsb.strAlcoValue;
import static com.workship.attachusb.AttachUsb.usbService;


public class SlideStartActivity extends AppCompatActivity {
    /* public ImageView mButton;
     public static CircleImageView mShowImg;*/
    public static TextView mTextView;
    int finalI=0;
    Handler handler;
    public static ProgressBar mProgressBar;
    private SharedPreferences mPreferences;
    Preview mPreview;
    public static SlideLayout mSlideLayout;
    Utils mUtils;

    /*  private final ServiceConnection usbConnection = new ServiceConnection() {
          @Override
          public void onServiceConnected(ComponentName arg0, IBinder arg1) {
              usbService = ((UsbService.UsbBinder) arg1).getService();
              usbService.setHandler(mHandler);
              mHandler = new MyHandler(SlideStartActivity.this);
          }

          @Override
          public void onServiceDisconnected(ComponentName arg0) {
              usbService = null;
          }
      };
  */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_slide_start);
        mPreview=new Preview(SlideStartActivity.this);
        mProgressBar = (ProgressBar) findViewById(R.id.slide_progress);
        mPreferences = PreferenceManager.getDefaultSharedPreferences(SlideStartActivity.this);
        mTextView = (TextView) findViewById(R.id.slider_msg_txt);
        SlideLayout slider = (SlideLayout) findViewById(R.id.sliderbtn);
        slider.setRenderer(new ScaleRenderer());
       mUtils = new Utils(SlideStartActivity.this);

        slider.setSlider(new HorizontalSlider());
        slider.addSlideListener(new ISlideListener() {
            @Override
            public void onSlideDone(SlideLayout slider, boolean done) {
                if (done) {
                    mSlideLayout=slider;
                    recordBacValue(slider);
                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTextView.setText("Please Swipe to Record Alcohol Value.");
    }

    private void recordBacValue(final SlideLayout slider){
        /*if value come from usb then it will be true else it will be false*/
            flagAlValue = false;
            mProgressBar.setVisibility(View.VISIBLE);
            mTextView.setText("");
        finalI=0;
            // if UsbService was correctly binded, Send data to Usb Port
            if (usbService != null) {
                String data = "$READ*#";
                usbService.write(data.getBytes());

/*used this loop for wait ALcohal value until 40 seconds*/


                     handler = new Handler();
                handler.postDelayed(mRunnable,2000);
                  /*  handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {



                        }
                    }, 2000);*/


            }
    }
Runnable mRunnable= new Runnable() {
    @Override
    public void run() {

        if (flagAlValue) {
           /* mProgressBar.setVisibility(View.GONE);
            Utils.openActivity(AlcoDetail.class);*/
            SharedPreferences.Editor mEditor = mPreferences.edit();
            mEditor.putString("AlcohalValue", strAlcoValue);
            mEditor.commit();
            mEditor.apply();
            finalI=0;
            /*This method is used to capture image and go to next activity*/
            mPreview.surfaceCreated(mPreview.mHolder);
            flagAlValue=false;

        }
        if (finalI == 20) {
            mSlideLayout.reset();
            mTextView.setText("Please Blow Again to Record Value.");
            mProgressBar.setVisibility(View.GONE);
            finalI=0;
            handler.removeCallbacks(mRunnable);
            if (usbService != null) {
                String data = "$STOP*#";
                usbService.write(data.getBytes());
            }
        }else{
            finalI=finalI+1;
            handler.postDelayed(mRunnable,2000);
        }


    }
};
        //   mButton.setOnTouchListener(mSwipeButton.getButtonTouchListener(mButton,mTextView));



    /*@Override
    public void onResume() {
        super.onResume();
        setFilters();  // Start listening notifications from UsbService
        startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it

    }*/
/*

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mUsbReceiver);
        unbindService(usbConnection);
    }
*/

    /*private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
        if (!UsbService.SERVICE_CONNECTED) {
            Intent startService = new Intent(this, service);
            if (extras != null && !extras.isEmpty()) {
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }*/

    /*private void setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }*/

    /*
     * This handler will be passed to UsbService. Data received from serial port is displayed through this handler
     */
   /* public static class MyHandler extends Handler {
        private final WeakReference<SlideStartActivity> mActivity;

        public MyHandler(SlideStartActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    String data = (String) msg.obj;
                //    Toast.makeText(getApplicationContext(),""+data,Toast.LENGTH_LONG).show();
                    *//*if(null!=data && !data.equals("")) {
                        String[] strArray = data.split(" ");
                        String strAlco = strArray[1];
                        String[] alcohalValue=strAlco.split("*");
                        mAlcohal.setText(data + "=="+alcohalValue[0]);
                    }*//*
                    mAlcohal.setText(data );
                  //  unregisterReceiver(mUsbReceiver);
                  //  unbindService(usbConnection);
                    //mActivity.get().display.append(data);
                    break;
            }
        }
    }*/

    /*
     * Notifications from UsbService will be received here.
     */
    /*
    * Notifications from UsbService will be received here.
    */
    @Override
    protected void onStart() {
        super.onStart();
        setFilters();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mUsbReceiver);
    }

    public void setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }
    public final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                    Utils.openActivity(SlideStartActivity.class);
                    Toast.makeText(context, "USB Ready.", Toast.LENGTH_SHORT).show();

                    break;
                case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                    Toast.makeText(context, "USB Permission not granted.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    Intent intent1 = new Intent(SlideStartActivity.this, AttachUsb.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    break;
                case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    Intent mIntent = new Intent(SlideStartActivity.this, AttachUsb.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent);
                    break;
                case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                    Intent mIntent1 = new Intent(SlideStartActivity.this, AttachUsb.class);
                    mIntent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent1);
                    Toast.makeText(context, "USB disconnected.", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                    Intent mIntent2 = new Intent(SlideStartActivity.this, AttachUsb.class);
                    mIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent2);
                    Toast.makeText(context, "USB device not supported", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


private void sendAlcoValue(){
    File file = new File(""/*imageUri.getPath()*/);
    RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
    RequestBody name = RequestBody.create(MediaType.parse("text/plain"), strAlcoValue);
    ApiInterface apiService =
            ApiClient.getClient().create(ApiInterface.class);

    Call<LoginModel> call = apiService.editUser( fbody, name);
    call.enqueue(new Callback<LoginModel>() {
        @Override
        public void onResponse(Call<LoginModel>call, Response<LoginModel> response) {
              /*  List<Movie> movies = response.body().getResults();
                Log.d(TAG, "Number of movies received: " + movies.size());*/
            Integer status=response.body().mStatus;
            String mMessage=response.body().mMessage;
            if(status==1){
                mUtils.openActivity(AttachUsb.class);
                mProgressBar.setVisibility(View.GONE);
                finish();
            }else{
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(SlideStartActivity.this, mMessage, Toast.LENGTH_SHORT).show();
            }
            //  mUtils.openActivity(WebActivity.class);
        }

        @Override
        public void onFailure(Call<LoginModel>call, Throwable t) {
            // Log error here since request failed
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(SlideStartActivity.this, "Please Try Again!", Toast.LENGTH_SHORT).show();
            Log.e("", t.toString());
        }
    });
}
}