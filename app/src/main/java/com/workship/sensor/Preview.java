package com.workship.sensor;

/**
 * Created by Vishal on 1/31/2018.
 */


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import com.workship.AlcoDetail.AlcoDetail;
import com.workship.Utils.Utils;
import com.workship.attachusb.AttachUsb;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Preview extends SurfaceView implements SurfaceHolder.Callback { // <1>
    private static final String TAG = "Preview";
    public static Bitmap mFinalBitmap;
    Context mContext;
    public SurfaceHolder mHolder;  // <2>
    public Camera camera; // <3>
    static int i = 5;

    Preview(Context context) {
        super(context);
        mContext = context;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();  // <4>
        mHolder.addCallback(this);  // <5>
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); // <6>

    }

    // Called once the holder is ready
    public void surfaceCreated(SurfaceHolder holder) {  // <7>
        // The Surface has been created, acquire the camera and tell it where
        // to draw.

        if (camera == null) {

            camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);

            // <8>
            camera.startPreview();
        }
        try {
            camera.setPreviewDisplay(holder);  // <9>

            Camera.Parameters parameters=camera.getParameters();

            Camera.Size pictureSize=getSmallestPictureSize(parameters);

            if ( pictureSize != null) {

                parameters.setPictureSize(pictureSize.width,
                        pictureSize.height);
                parameters.setPictureFormat(ImageFormat.JPEG);
                camera.setParameters(parameters);

            }


            camera.setPreviewCallback(new PreviewCallback() { // <10>
                // Called for each frame previewed
                public void onPreviewFrame(byte[] data, Camera camera) {  // <11>
                    Log.d(TAG, "onPreviewFrame called at: " + System.currentTimeMillis());
                    Preview.this.invalidate();  // <12>
                }
            });

            this.camera.takePicture(null, null, jpegCallback);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Called when the holder is destroyed
    public void surfaceDestroyed(SurfaceHolder holder) {  // <14>
        camera.stopPreview();
        camera = null;
    }

    // Called when holder has changed
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) { // <15>
        camera.startPreview();
    }
    public void onClick(View v) { // <5>
        camera.takePicture(shutterCallback, rawCallback, jpegCallback);
    }
    // Called when shutter is opened
    ShutterCallback shutterCallback = new ShutterCallback() { // <6>
        public void onShutter() {
            Log.d(TAG, "onShutter'd");
        }
    };
    // Handles data for raw picture
    PictureCallback rawCallback = new PictureCallback() { // <7>
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.d(TAG, "onPictureTaken - raw");
        }
    };
    // Handles data for jpeg picture
    PictureCallback jpegCallback = new PictureCallback() { // <8>
        public void onPictureTaken(byte[] data, Camera camera) {
            try {
                Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 20, stream);
                Bitmap mBitmap = RotateBitmap(bmp, 270);

                mFinalBitmap = mBitmap;
                /*Open Activity after */
                SlideStartActivity.mProgressBar.setVisibility(View.GONE);
                Utils.openActivity(AlcoDetail.class);
                SlideStartActivity.mSlideLayout.reset();
              //  Toast.makeText(mContext, "dsgfw==" + mFinalBitmap, Toast.LENGTH_LONG).show();
                //  SlideStartActivity.mShowImg.setImageBitmap(mBitmap);
                surfaceDestroyed(mHolder);
                Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length);
            } catch (Exception e) { // <10>
                e.printStackTrace();
            }
            Log.d(TAG, "onPictureTaken - jpeg");
        }
    };
    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
        Camera.Size result=null;

        for (Camera.Size size : parameters.getSupportedPictureSizes()) {
            if (result == null) {
                result=size;
            }
            else {
                int resultArea=result.width * result.height;
                int newArea=size.width * size.height;

                if (newArea < resultArea) {
                    result=size;
                }
            }
        }

        return(result);
    }
}


