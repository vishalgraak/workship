package com.workship.startengine;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.workship.R;
import com.workship.Utils.Utils;
import com.workship.attachusb.AttachUsb;
import com.workship.sensor.SlideStartActivity;
import com.workship.sensor.UsbService;

import static com.workship.attachusb.AttachUsb.usbService;

public class StartEngine extends AppCompatActivity {
ImageView mImageView;
private boolean startEngine=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_start_engine);
        mImageView=(ImageView)findViewById(R.id.start_engine);
        mImageView.setImageResource(R.mipmap.ic_power_off_icon);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usbService != null) { // if UsbService was correctly binded, Send data
                 if(startEngine) {
                     startEngine=false;
                     mImageView.setImageResource(R.mipmap.ic_power_off_icon);
                     String data = "$4 OFF*#";
                     usbService.write(data.getBytes());
                     Toast.makeText(getApplicationContext(), "Engine Stopped", Toast.LENGTH_SHORT).show();

                 }else{
                     startEngine=true;
                     mImageView.setImageResource(R.mipmap.ic_power_on_icon);
                     String data = "$4 ON*#";
                     usbService.write(data.getBytes());
                     Toast.makeText(getApplicationContext(), "Engine Started", Toast.LENGTH_SHORT).show();
                 }

                 /*Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 7);*/
                  /*  mPreview.surfaceCreated(mPreview.mHolder);*/
                    //   mPreview.surfaceDestroyed(mPreview.mHolder);
                    //   mPreview.onClick(v);
                }
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();

        setFilters();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mUsbReceiver);
    }

    public void setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }
    public final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                    Utils.openActivity(SlideStartActivity.class);
                    Toast.makeText(context, "USB Ready.", Toast.LENGTH_SHORT).show();

                    break;
                case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                    Toast.makeText(context, "USB Permission not granted.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    Intent intent1 = new Intent(StartEngine.this, AttachUsb.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    break;
                case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    Intent mIntent = new Intent(StartEngine.this, AttachUsb.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent);
                    break;
                case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                    Intent mIntent1 = new Intent(StartEngine.this, AttachUsb.class);
                    mIntent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent1);
                    Toast.makeText(context, "USB disconnected.", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                    Intent mIntent2 = new Intent(StartEngine.this, AttachUsb.class);
                    mIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent2);
                    Toast.makeText(context, "USB device not supported", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
