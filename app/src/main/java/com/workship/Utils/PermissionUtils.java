package com.workship.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

/**
 * Created by Vishal on 1/30/2018.
 */

public class PermissionUtils {
    public  static final int RequestPermissionCode  = 1 ;
    Activity mActivity;
    public PermissionUtils(Activity activity) {
        mActivity=activity;
    }

    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                Manifest.permission.CAMERA))
        {

            Toast.makeText(mActivity,"CAMERA permission is mandetory to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(mActivity,new String[]{
                    Manifest.permission.CAMERA}, RequestPermissionCode);

        }
    }

    /*@Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(mActivity,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(mActivity,"Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
*/
}

