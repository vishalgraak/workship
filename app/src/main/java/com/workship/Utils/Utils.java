package com.workship.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.workship.Utils.PermissionUtils.RequestPermissionCode;

public class Utils {
    static Activity mActivity;
    public Utils(Activity mContext){
        mActivity=mContext;
    }
    public static  void openActivity(Class webActivityClass){
        Intent mIntent=new Intent(mActivity,webActivityClass);
        mActivity.startActivity(mIntent);
    }


    public boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 6;
    }
    /*public void setToolbar(Toolbar toolbar Context m) {
        mActivity.setTitle("");
        toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_image);
        .setSupportActionBar(toolbar);
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Booked Tests");
        String title = mTitle.getText().toString();

    }*/


    public boolean checkPermission() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(mActivity.getApplicationContext(), CAMERA);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(mActivity.getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED;
    }
}