package com.workship.AlcoDetail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.workship.R;
import com.workship.Utils.Utils;
import com.workship.attachusb.AttachUsb;
import com.workship.sensor.Preview;
import com.workship.sensor.SlideStartActivity;
import com.workship.sensor.UsbService;
import com.workship.startengine.StartEngine;

import de.hdodenhof.circleimageview.CircleImageView;

public class AlcoDetail extends AppCompatActivity {
    private TextView mAlcohalTxt, mRestTxt, mNextTxt;
    private ImageView mAlcohalTickImg, mRestTickImg;
    private Utils mUtils;
    private SharedPreferences mPreferences;
    public String strAlcoValue;
    private CircleImageView mCircleImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_alco_detail);
        mUtils = new Utils(this);
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        strAlcoValue = mPreferences.getString("AlcohalValue", "");
        mAlcohalTxt = (TextView) findViewById(R.id.alco_value_txt);
        mRestTxt = (TextView) findViewById(R.id.rest_value_txt);
        mNextTxt = (TextView) findViewById(R.id.next_txt);
        mCircleImageView = (CircleImageView) findViewById(R.id.user_image);
        mAlcohalTickImg = (ImageView) findViewById(R.id.alco_right_img);
        mRestTickImg = (ImageView) findViewById(R.id.rest_tick_img);
       // mAlcohalTxt.setText(strAlcoValue);
            mCircleImageView.setImageBitmap(Preview.mFinalBitmap);

            if (!strAlcoValue.equals("0.000") ) {
                mNextTxt.setClickable(false);
                mAlcohalTxt.setText("BAC "+strAlcoValue);
            }else{
                mAlcohalTxt.setText("BAC "+strAlcoValue);
            }

        mNextTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strAlcoValue.equals("0.000") ) {
                    Utils.openActivity(StartEngine.class);
                    finish();
                }else{
                    Toast.makeText(AlcoDetail.this, "You don't have permission to start your Ride.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        setFilters();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mUsbReceiver);
    }

    public void setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }
    public final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                    Utils.openActivity(SlideStartActivity.class);
                    Toast.makeText(context, "USB Ready.", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                    Toast.makeText(context, "USB Permission not granted.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    Intent intent1 = new Intent(AlcoDetail.this, AttachUsb.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    break;
                case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    Intent mIntent = new Intent(AlcoDetail.this, AttachUsb.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent);
                    break;
                case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                    Intent mIntent1 = new Intent(AlcoDetail.this, AttachUsb.class);
                    mIntent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent1);
                    Toast.makeText(context, "USB disconnected.", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                    Intent mIntent2 = new Intent(AlcoDetail.this, AttachUsb.class);
                    mIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent2);
                    Toast.makeText(context, "USB device not supported", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
